package pri.sean10202.shop

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        sign_up.setOnClickListener {
            val sEmail = ed_email.text.toString()
            val sPassword = ed_password.text.toString()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(sEmail, sPassword)
                .addOnCompleteListener {
                if(it.isSuccessful) {
                    AlertDialog.Builder(this)
                        .setTitle("Success")
                        .setMessage("The account is created successfully, you can log in by using it.")
                        .setPositiveButton("OK") { dialog, which ->
                            setResult(Activity.RESULT_OK)
                            finish()
                        }.show()
                    startActivity(Intent(this, NicknameActivity::class.java))
                } else {
                    AlertDialog.Builder(this)
                        .setTitle("Failure")
                        .setMessage(it.exception?.message)
                        .setPositiveButton("OK", null)
                        .show()
                }
            }
        }
    }
}

package pri.sean10202.shop

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_nickname.*

class NicknameActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nickname)
        done.setOnClickListener {
            setNickname(ed_nickname.text.toString())

            FirebaseDatabase.getInstance().getReference("User")
                .child(FirebaseAuth.getInstance().currentUser!!.uid)
                .child("Nickname")
                .setValue(ed_nickname.text.toString())
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
